# QMapWidget #

A simple map widget for Qt.

### How to use the widget? ###

~~~~
/* Inside header file: */
#include <QMapWidget/QMapWidget>  

class MainWindow : public QMainWindow
{
private:
    QMapWidget * map;  
}

/* Inside constructor: */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    map = new QMapWidget(this);
    setCentralWidget(map);
    map->setFocus();
}
~~~~

### Example apps ###

Windows: [QMapWidgetTest_Qt.7z](http://marianmmunteanu.appspot.com/files/QMapWidgetTest_Qt.7z)  
![](http://3.bp.blogspot.com/-Bkycb0g1yFc/VDWZKhoiY2I/AAAAAAAAA9k/RzLOGLIVWLs/s1600/app_win.PNG)  
  
Android: [QMapWidgetTest.apk](http://marianmmunteanu.appspot.com/files/QMapWidgetTest.apk)  
![](http://2.bp.blogspot.com/-WChlXwXwcs0/VDWnG6zEdJI/AAAAAAAAA90/PJdvdyWgW1E/s1600/IMG_1736.png)

![](http://marian.host.sk/blog/logo.php?logo=logoMap.png)
![](http://munteanu-marian.appspot.com/logo.png?logo=Map.png)

The code is based on [Qt lightmap example](https://qt.gitorious.org/qt/qtbase/source/ec68f67ee55944e1f05bdbe1362832ee2ab156c7%3Aexamples/embedded/lightmaps)